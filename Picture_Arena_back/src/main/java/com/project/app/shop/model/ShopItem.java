package com.project.app.shop.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ShopItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer userId;
    private Integer cardId;
    private String urlImg;
    private int price;
    
    public ShopItem() {
    }

    public ShopItem(Integer userId, Integer cardId, String urlImg, int price) {
        this.userId = userId;
        this.cardId = cardId;
        this.urlImg = urlImg;
        this.price = price;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public Integer getId() {
        return id;
    }

    public Integer getUserId() {
        return userId;
    }

    public Integer getCardId() {
        return cardId;
    }

	public String getUrlImg() {
		return urlImg;
	}

	public void setUrlImg(String urlImg) {
		this.urlImg = urlImg;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "ShopItem [id=" + id + ", userId=" + userId + ", cardId=" + cardId + ", urlImg=" + urlImg + ", price="
				+ price + "]";
	}
	
	
	
}

