package com.project.app.arena.controller;

import java.util.Optional;

import org.springframework.data.geo.Point;
import org.springframework.data.repository.CrudRepository;

import com.project.app.arena.model.Arena;

public interface ArenaRepository extends CrudRepository<Arena, Integer>{

	public Optional<Arena> findByLocation(Point location);
	
}
