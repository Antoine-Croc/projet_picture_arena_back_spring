package com.project.app.arena.model;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.geo.Point;

@Entity
@Table(name = "ARENA")
public class Arena {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int arenaId;
	private Point location;
	private int pointRED;
	private int pointBLUE;
	private int pointGREEN;
	private String factionCtrl;
	
	@ElementCollection
	private List<Integer> challengeIdList;
	
	
	public Arena() {}
	
	public Arena(Point location, List<Integer> challengeIdList) {
		this.location = location;
		this.challengeIdList = challengeIdList;
		this.pointRED= 0;
		this.pointBLUE = 0;
		this.pointGREEN = 0;
		this.factionCtrl = "";
	}
		
	public int getArenaId() {
		return arenaId;
	}

	public void setArenaId(int arenaId) {
		this.arenaId = arenaId;
	}

	public List<Integer> getChallengeIdList() {
		return challengeIdList;
	}

	public void setChallengeIdList(List<Integer> challengeIdList) {
		this.challengeIdList = challengeIdList;
	}

	public Point getLocation() {
		return location;
	}

	public void setLocation(Point location) {
		this.location = location;
	}

	public int getPointRED() {
		return pointRED;
	}

	public void setPointRED(int pointRED) {
		this.pointRED = pointRED;
	}

	public int getPointBLUE() {
		return pointBLUE;
	}

	public void setPointBLUE(int pointBLUE) {
		this.pointBLUE = pointBLUE;
	}

	public int getPointGREEN() {
		return pointGREEN;
	}

	public void setPointGREEN(int pointGREEN) {
		this.pointGREEN = pointGREEN;
	}

	public String getFactionCtrl() {
		return factionCtrl;
	}

	public void setFactionCtrl(String factionCtrl) {
		this.factionCtrl = factionCtrl;
	}	
}
