package com.project.app.arena.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.app.arena.controller.ArenaService;
import com.project.app.arena.model.Arena;
import com.project.app.challenge.model.Challenge;

@RestController
@RequestMapping("/arena")
public class ArenaRstCrt {
	
	@Autowired
	ArenaService aService;
	
	@RequestMapping(method=RequestMethod.GET,value="/")
	public Arena getArena(@RequestParam String lat, String lon) {
		Double latD = Double.parseDouble(lat);
		Double lonD = Double.parseDouble(lon);
		Point p = new Point(latD,lonD);
		return aService.getArena(p);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/") 
	public boolean addArena(@RequestParam String lat, @RequestParam String lon) {
		Double latD = Double.parseDouble(lat);
		Double lonD = Double.parseDouble(lon);
		Point p = new Point(latD,lonD);
		return aService.addArena(p);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/")
	public boolean delArena(@RequestParam String lat,@RequestParam String lon) {
		Double latD = Double.parseDouble(lat);
		Double lonD = Double.parseDouble(lon);
		Point p = new Point(latD,lonD);
		return aService.removeArena(p);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/all/")
	public List<Arena> getAllArenas() {
		return aService.getAllArenas();
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/challenge/")
	public List<Integer> getChallenges(@RequestParam String lat,@RequestParam String lon) {
		Double latD = Double.parseDouble(lat);
		Double lonD = Double.parseDouble(lon);
		Point p = new Point(latD,lonD);
		return aService.getChallengeIds(p);
	}
	
	/*@RequestMapping(method=RequestMethod.POST,value="/challenge/") 
	public boolean addChallenge(@RequestParam String lat, String lon, int challId) {
		Point p = new Point(Long.parseLong(lat),Long.parseLong(lon));
		return aService.addChallengeId(p, challId);
	}*/
	
	@RequestMapping(method=RequestMethod.DELETE,value="/challenge/")
	public boolean delArena(@RequestParam  String lat, @RequestParam String lon,@RequestParam int challId) {
		Double latD = Double.parseDouble(lat);
		Double lonD = Double.parseDouble(lon);
		Point p = new Point(latD,lonD);
		return aService.removeChallengeId(p, challId);
	}
}
