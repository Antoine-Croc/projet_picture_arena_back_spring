package com.project.app.arena.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;

import com.project.app.arena.model.Arena;

@Service
public class ArenaService {
	
	private final ArenaRepository aRepository;
	
	public ArenaService(ArenaRepository aRepository) {
		this.aRepository = aRepository;
	}

	public Arena getArena(Point loc) {
		Optional<Arena> aOpt = aRepository.findByLocation(loc);
		if (aOpt.isPresent()) {
			return aOpt.get();
		}
		System.out.println("No Arena in that location");
		return null;
	}
	
	public Arena getArenaWithId(int id) {
		Optional<Arena> aOpt = aRepository.findById(id);
		if (aOpt.isPresent()) {
			return aOpt.get();
		}
		System.out.println("No Arena in that location");
		return null;
	}
	
	public boolean addArena(Point loc) {
		Optional<Arena> aOpt = aRepository.findByLocation(loc);
		if (!aOpt.isPresent()) {
			Arena a = new Arena();
			a.setLocation(loc);
			aRepository.save(a);
			System.out.println("New arena at location: " + loc.toString());
			return true;			
		}
		System.out.println("Existing arena in that location");
		return false;
	}
	
	public boolean removeArena(Point loc) {
		Optional<Arena> aOpt = aRepository.findByLocation(loc);
		if (aOpt.isPresent()) {
			Arena a = aOpt.get();
			aRepository.delete(a);
			System.out.println("Arena deleted at location: " + loc.toString());
			return true;
		}
		System.out.println("No arena at that location");
		return false;
	}
	
	public List<Arena> getAllArenas() {
		List<Arena> aA = new ArrayList<>();
		aRepository.findAll().forEach(aA::add);
		return aA;
	}
	
	public List<Integer> getChallengeIds(Point loc) {
		Optional<Arena> aOpt = aRepository.findByLocation(loc);
		if (aOpt.isPresent()) {
			List<Integer> cL = aOpt.get().getChallengeIdList();
			return cL;
		}
		System.out.println("Wrong arena location");
		return null;
	}
	
	public boolean addChallengeId(Arena a, int challId) {
		List<Integer> cL = a.getChallengeIdList();
		cL.add(challId);
		a.setChallengeIdList(cL);
		aRepository.save(a);
		return true;
	}
	
	public boolean removeChallengeId(Point loc, int challId) {
		Optional<Arena> aOpt = aRepository.findByLocation(loc);
		if (aOpt.isPresent()) {
			Arena a = aOpt.get();
			List<Integer> cL = a.getChallengeIdList();
			cL.remove(cL.indexOf(challId));
			a.setChallengeIdList(cL);
			aRepository.save(a);
			return true;
		}
		System.out.println("Wrong arena location");
		return false;
	}
	
	public void addPointInArena(int id, int point, String faction) {
		Arena arene = getArenaWithId(id);
		if(faction.equals("GREEN")) { arene.setPointGREEN(point+arene.getPointGREEN()); }
		if(faction.equals("RED")) { arene.setPointRED(point+arene.getPointRED()); }
		if(faction.equals("BLUE")) { arene.setPointBLUE(point+arene.getPointBLUE()); }
		
		if(arene.getPointRED() > arene.getPointBLUE() && arene.getPointRED() > arene.getPointGREEN()) {
			arene.setFactionCtrl("RED");
		}
		else if(arene.getPointGREEN() > arene.getPointBLUE() &&  arene.getPointGREEN()> arene.getPointRED()) {
			arene.setFactionCtrl("GREEN");
		}
		else if( arene.getPointBLUE() > arene.getPointGREEN() &&  arene.getPointBLUE()> arene.getPointRED()) {
			arene.setFactionCtrl("BLUE");
		}
		else {arene.setFactionCtrl(""); }
		
		aRepository.save(arene);
	}
}
