package com.project.app.challenge.model;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CHALLENGE")
public class Challenge {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private int arenaId;
	private int userId;
	private int bet;
	private String username;
	private String faction;


	@ElementCollection
	private List<Integer> UserTeam;
	

	@ElementCollection
	private List<Integer> UserItems;
	
	public Challenge() {}
	
	public Challenge(int arenaId, int userId, List<Integer> userTeam, List<Integer> UserItems, int bet, String username, String faction) {
		this.arenaId = arenaId;
		this.userId = userId;
		this.UserTeam = userTeam;
		this.UserItems = UserItems;
		this.bet = bet;
		this.username = username;
		this.faction = faction;
	}

	public int getId() {
		return id;
	}
	
	public int getArenaId() {
		return arenaId;
	}

	public void setArenaId(int arenaId) {
		this.arenaId = arenaId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}	
	public int getBet() {
		return bet;
	}

	public void setBet(int bet) {
		this.bet = bet;
	}
	public List<Integer> getUserTeam() {
		return UserTeam;
	}

	public void setUserTeam(List<Integer> userTeam) {
		UserTeam = userTeam;
	}

	public List<Integer> getUserItems() {
		return UserItems;
	}

	public void setUserItems(List<Integer> userItems) {
		UserItems = userItems;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFaction() {
		return faction;
	}

	public void setFaction(String faction) {
		this.faction = faction;
	}
	
}
