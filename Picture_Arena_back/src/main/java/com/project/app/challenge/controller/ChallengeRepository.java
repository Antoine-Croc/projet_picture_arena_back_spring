package com.project.app.challenge.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.project.app.challenge.model.Challenge;

public interface ChallengeRepository extends CrudRepository<Challenge, Integer>{
	
	List<Challenge> findByArenaId(int arenaId);
	List<Challenge> findByUserId(int userId);
	Optional<Challenge> findById(int id);
	
}
