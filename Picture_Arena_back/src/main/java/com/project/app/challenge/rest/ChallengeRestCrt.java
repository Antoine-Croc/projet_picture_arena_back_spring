package com.project.app.challenge.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.app.challenge.controller.ChallengeService;
import com.project.app.challenge.model.Challenge;


@RestController
@RequestMapping("/challenge")
public class ChallengeRestCrt {

	@Autowired
	ChallengeService cService;
	
	@RequestMapping(method=RequestMethod.GET,value="/")
	public List<Challenge> getChallenges() {
		return cService.getAllChallenges();
	}

	
	@RequestMapping(method=RequestMethod.GET,value="/arena/")
	public List<Challenge> getChallengesByArena(@RequestParam String lat, @RequestParam String lon) {
		Double latD = Double.parseDouble(lat);
		Double lonD = Double.parseDouble(lon);
		Point p = new Point(latD,lonD);
		return cService.getAllChallenges();
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/")
	public boolean addChallenge(@RequestParam int arenaId,@RequestParam int userId,@RequestParam int bet) {
		return cService.addChallenge(arenaId, userId,bet);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/{challid}/")
	public Challenge getChallenge(@PathVariable int challid) {
		return cService.getChallenge(challid);
	}

	/*
	@RequestMapping(method=RequestMethod.DELETE,value="/{challid}/")
	public boolean removeChallenge(@PathVariable int challid) {
		return cService.removeChallenge(challid);
	}
	*/
	@RequestMapping(method=RequestMethod.DELETE,value="/{challid}/validate")
	public boolean finishChallenge(@PathVariable int challid, @RequestParam int idWinner, @RequestParam int idLooser,@RequestParam String faction) {
		return cService.finishChallenge(challid,idWinner,idLooser, faction);
	}
}
