package com.project.app.transaction.controller;

import com.project.app.shop.controller.ShopService;
import com.project.app.shop.model.ShopItem;
import com.project.app.transaction.model.CarteDTO;
import com.project.app.transaction.model.Transaction;
import com.project.app.transaction.model.UserDTO;
import com.project.app.user.controller.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionService {
    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    UserService uService;
    
    @Autowired
    ShopService shopService;


    //Ajouter la transaction
    public void addTransaction(Transaction transaction) {
        //ajouter la transaction
        transactionRepository.save(transaction);

        System.out.println("add transaction");
        System.out.println(transaction);
    }

    //retourner toutes les transactions
    public List<Transaction> getAllTransactions() {
        List<Transaction> transactions = new ArrayList<>();
        transactionRepository.findAll().forEach(transactions::add);
        return transactions;
    }

    //retourner la transaction dont id = ..
    public Transaction getTransactionById(int id) {
        Optional<Transaction> tOpt = transactionRepository.findById(id);
        if (tOpt.isPresent()) {
            return tOpt.get();
        }else {
            return null;
        }
    }

    //terminer la transaction(vendue)
    public Boolean closeTransaction(int idAcheteur, int idShopItem) {
    	Boolean ret = false;
    	ShopItem tradePropose = shopService.getShopItemById(idShopItem);
        
        int idVendeur = tradePropose.getUserId();
        int idCarte = tradePropose.getCardId();
        int price = tradePropose.getPrice();
        
        int portefeuille = getUserPortefeuille(idAcheteur);

        if( portefeuille>=price ) {
            //set portefeuille
            putUserPortefeuille(idVendeur, price);
            putUserPortefeuille(idAcheteur, -1*price);

            //add card on acheteur
            postUserCarte(idAcheteur,idCarte);
            
            //changer owner
            putOwnerCarte( idAcheteur, idCarte);

            //update transaction state
            Transaction transaction = new Transaction(idVendeur,idCarte,idAcheteur);
            transaction.setClosed();
            deleteShopItem(tradePropose);
            addTransaction(transaction);
            System.out.println("close transaction: OK");
            ret = true;
        }
        return ret;
    }


    private int getUserPortefeuille(int id) {
    	// Micro-Service CASE
        //ResponseEntity<UserDTO> resp = new RestTemplate().getForEntity("http://54.152.148.26:80/users/"+id, UserDTO.class);
        //return resp.getBody().getPortefeuille();
    	
    	//Mono Case :
    	return uService.getMoneyWithId(id);
    	
    }

    private void putUserPortefeuille(int id, int price) {
    	uService.modifyMoneyId(price, id);
    }

    private void postUserCarte(int idUser, int idCarte) {
        //new RestTemplate().exchange("http://54.152.148.26:80/users/"+idUser+"/cartes?idCarte="+idCarte, HttpMethod.POST, null, Void.class);
    	uService.addCardToUserWithId(idUser, idCarte);
    }
    
    private void putOwnerCarte(int idOwner, int idCarte) {
        new RestTemplate().exchange("http://204.236.239.109:80/card/"+idCarte+"?idOwner="+idOwner, HttpMethod.PUT, null, Void.class);
    }

    private void deleteShopItem(ShopItem deleteShop) {
    	shopService.deleteShopItem(deleteShop);
    }
}
