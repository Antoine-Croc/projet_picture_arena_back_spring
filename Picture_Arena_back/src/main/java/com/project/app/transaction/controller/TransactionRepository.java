package com.project.app.transaction.controller;

import java.util.List;

import com.project.app.transaction.model.Transaction;
import org.springframework.data.repository.CrudRepository;


public interface TransactionRepository extends CrudRepository<Transaction, Integer>{


	public List<Transaction> findByIdVendeur(int idVendeur);
	public List<Transaction> findByIdAcheteur(int idVendeur);
	public List<Transaction> findByIdCarte(int idCarte);

}
