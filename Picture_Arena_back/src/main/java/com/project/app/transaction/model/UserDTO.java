package com.project.app.transaction.model;

public class UserDTO {
    public int portefeuille;

    public UserDTO() {}

    public int getPortefeuille() {
        return portefeuille;
    }

    public void setPortefeuille(int portefeuille) {
        this.portefeuille = portefeuille;
    }

}
