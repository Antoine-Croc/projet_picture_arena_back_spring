package com.project.app.transaction.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Transaction {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    private int idCarte;
    private int idVendeur;
    private int idAcheteur;
    private boolean isClosed = false;


    public Transaction() {}

    public Transaction( int idVendeur,int idCarte,int idAcheteur) {
        super();
        this.idVendeur = idVendeur;
        this.idCarte = idCarte;
        this.idAcheteur =idAcheteur;
    }

    public int getIdAcheteur() {
        return idAcheteur;
    }

    public void setIdAcheteur(int idAcheteur) {
        this.idAcheteur = idAcheteur;
    }

    public int getIdVendeur() {
        return idVendeur;
    }

    public int getIdCarte() {
        return idCarte;
    }
    
    public boolean getCloseed() {
    	return isClosed;
    }
    
    public void setClosed(){
        this.isClosed=true;
    }

    @Override
    public String toString() {
        return "[ Transaction: "+this.id+
                ", Id Vendeur: "+this.idVendeur +
                ", Id Carte: "+this.idCarte + ",Id Acheteur" +this.idAcheteur+" ]";
    }

}
